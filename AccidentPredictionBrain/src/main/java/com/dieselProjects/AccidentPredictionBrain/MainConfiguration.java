package com.dieselProjects.AccidentPredictionBrain;

import com.dieselProjects.AccidentPredictionBrain.APTypes.APActivationType;
import com.dieselProjects.AccidentPredictionBrain.APTypes.APNetworkType;
import com.dieselProjects.AccidentPredictionBrain.APTypes.ROUTE;

public class MainConfiguration {

	private static MainConfiguration instance = null;

	protected MainConfiguration() {
		// Exists only to defeat instantiation.
	}

	public static MainConfiguration getInstance() {
		if (instance == null) {
			instance = new MainConfiguration();
		}
		return instance;
	}

	// basic configuration:
	public String route = "garden_state_parkway";

	
	// configuration for APFarm:
	//public String location = "gsp_140_mi";
	//public String networkHighLow = "normal";
	public String location = "gsp_145_mi";
	public String networkHighLow = "normal";
	public String networkName = location + "-" + networkHighLow;
	
	public String farmDirLocation = "Res/Engines";
	
	
	public ROUTE routeForUse = ROUTE.GSP;
	// Legend:
	// [0] - gsp_140_mi
	// [1] - us206_x_mi
	// [2] - us_9_x_mi
	// [3] - i80_55_mi
	// [4] - us1_x_mi
	// [5] - nj17_x_mi
	
	public String[] routeFileLocation = {
			//"Res/gsp_140_S.csv",
			"Res/gsp_145m_data.csv",
			"Res/us_206_data.csv",
			"Res/us9_data.csv",
	 		"Res/i80_55m_data.csv",
	 		"Res/us_1_data.csv",
	 		"Res/nj_17_5m_data.csv"};

	
	
	public String keyVectorFile = "Res/gsp_140_S.csv";
	
	public String benchMarkFile = "Res/benchMarkResults.csv";
	public String benchMarkCombineFile = "Res/benchMarkCombineResults.csv";


	// scenario configuration:
	public int scenario = 4;
	
	// scenario 3 configuration:
	public int iterationForScenario = 1;

	// Genetic Algorithm configuration:
	public boolean useGA = false;
	public int numberOfParametersForGA = 3;
	public int populationSize = 4;
	public int elitism = 1;
	public int fitnessTarget = 80;
	public int maxNumberOfGeneration = 4;
	public double mutationProbability = 0.05d;

	// Neural Net configuration:
	public int numOfTrainingTick = 365 * 24 * 6;
	public int maxTrainingIterations = 400;
	public double learningRate = 0.00001;
	public double momentum = 0.1;
	public double outputTrueTH = 0.5;
	public double inputNegativeValue = 0.0;

	public int outputTH = 1;
	public double trueFalseFactor = 2.0;

	// NN parameters:
	public APNetworkType networkType = APNetworkType.BASIC_FF;
	public APActivationType activationType = APActivationType.RESILIENT;
	
	public int hiddenLayerWidth = 12;

	public String print() {
		
		String configurationString = "\n";
		configurationString += "Scenario number: " + scenario + "\n";
		configurationString += "number of training ticks: " + numOfTrainingTick
				+ "\n";
		return configurationString;
		
	}

}
