package com.dieselProjects.AccidentPredictionBrain.InputVector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class APIdealConfiguration {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	
	int idealTH;
	
	public APIdealConfiguration(){
		
		// default configuration:
		idealTH = cfg.outputTH;
		
	}
	
	
	public int getIdealTH(){
		return idealTH;
	}
	
}
