package com.dieselProjects.AccidentPredictionBrain.InputVector;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class APWeatherCondition {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	private double negValue = cfg.inputNegativeValue;

	double[] weatherCondition;

	public APWeatherCondition(String[] line,
			APWeatherConditionConfiguration weatherConditionConfiguration) {

		// TODO: all indexes should be parametric
		// visibility:
		if (weatherConditionConfiguration.getVisibility()) {
			double[] visibility = new Visibility(Double.valueOf(line[4]),
					weatherConditionConfiguration.getVisibilityTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					visibility);
		}

		if (weatherConditionConfiguration.getPrecipitation()) {
			double[] precipitation = new Precipitation(Double.valueOf(line[3]),
					weatherConditionConfiguration.getPrecipitationTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					precipitation);
		}

		// rain:
		if (weatherConditionConfiguration.getRain()) {
			double[] rain = new Rain(line[12].equals("1"),
					line[13].equals("1"), line[14].equals("1"),
					line[15].equals("1"), line[16].equals("1"),
					line[17].equals("1"),
					weatherConditionConfiguration.getRainTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					rain);
		}

		// snow
		if (weatherConditionConfiguration.getSnow()) {
			double[] snow = new Snow(line[18].equals("1"),
					line[19].equals("1"), line[20].equals("1"),
					line[21].equals("1"),
					weatherConditionConfiguration.getSnowTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					snow);
		}

		// fog
		if (weatherConditionConfiguration.getFog()) {
			double[] fog = new Fog(line[9].equals("1"), line[10].equals("1"),
					line[11].equals("1"),
					weatherConditionConfiguration.getFogTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					fog);
		}

		// wind:
		if (weatherConditionConfiguration.getWind()) {
			double[] wind = new Wind(Double.valueOf(line[5]),
					Double.valueOf(line[7]), Double.valueOf(line[6]),
					weatherConditionConfiguration.getWindTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					wind);
		}
		
		if (weatherConditionConfiguration.getWindExt()){
			
			double[] windExt = new WindExt(Double.valueOf(line[5]),
					Double.valueOf(line[7]), Double.valueOf(line[6]),
					weatherConditionConfiguration.getWindExtTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					windExt);

		}

		// hail:
		if (weatherConditionConfiguration.getHail()) {
			double[] hail = new Hail(line[33].equals("1"),
					line[34].equals("1"), line[35].equals("1"),
					weatherConditionConfiguration.getHailTH()).get();
			this.weatherCondition = ArrayUtils.addAll(this.weatherCondition,
					hail);
		}

	}

	public double[] get() {

		return this.weatherCondition;

	}

	private class Visibility {

		private int BIT_VECTOR_WIDTH;

		// number of bits to set
		int visibility;

		public Visibility(double visibility, String visibilityTH) {

			String[] visiTH = visibilityTH.split(" ");
			this.BIT_VECTOR_WIDTH = visiTH.length;

			this.visibility = (visibility < Double.valueOf(visiTH[0])) ? 1 : 0;

			if (this.BIT_VECTOR_WIDTH > 1) {
				this.visibility = (visibility < Double.valueOf(visiTH[1])) ? 2
						: this.visibility;
			}

		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;

			bitVector[0] = (this.visibility > 0) ? 1.0 : bitVector[0];
			if (this.BIT_VECTOR_WIDTH > 1) {
				bitVector[1] = (this.visibility > 1) ? 1.0 : bitVector[1];
			}
			return bitVector;

		}
	}

	private class Precipitation {

		private int BIT_VECTOR_WIDTH;

		int precipitation;

		public Precipitation(double precipitation, String precipitationTH) {

			String[] preTH = precipitationTH.split(" ");
			this.BIT_VECTOR_WIDTH = preTH.length;
			this.precipitation = (precipitation > Double.valueOf(preTH[0])) ? 1
					: 0;

			if (this.BIT_VECTOR_WIDTH > 1) {
				// setting index 1
				this.precipitation = (precipitation > Double.valueOf(preTH[1])) ? 2
						: this.precipitation;

			}

		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;

			bitVector[0] = (this.precipitation > 0) ? 1.0 : bitVector[0];
			if (BIT_VECTOR_WIDTH > 1) {
				bitVector[1] = (this.precipitation > 1) ? 1.0 : bitVector[1];
			}
			return bitVector;

		}

	}

	private class Rain {

		private int BIT_VECTOR_WIDTH;

		// number of bits to set
		int rain;

		// Legend for rain:
		// no rain - 0
		// lt_rain - 1
		// mod_rain - 2
		// hvy_rain - 3
		// lt_frz_rain - 4
		// mod_frz_rain - 5
		// hvy_frz_rain - 6
		int rainIndex;

		public Rain(boolean lt_rain, boolean mod_rain, boolean hvy_rain,
				boolean lt_frz_rain, boolean mod_frz_rain,
				boolean hvy_frz_rain, String rainTH) {

			String[] raTH = rainTH.split(" ");
			BIT_VECTOR_WIDTH = raTH.length;

			this.rainIndex = (lt_rain) ? 1 : 0;
			this.rainIndex = (mod_rain) ? 2 : this.rainIndex;
			this.rainIndex = (hvy_rain) ? 3 : this.rainIndex;
			this.rainIndex = (lt_frz_rain) ? 4 : this.rainIndex;
			this.rainIndex = (mod_frz_rain) ? 5 : this.rainIndex;
			this.rainIndex = (hvy_frz_rain) ? 6 : this.rainIndex;

			this.rain = (rainIndex >= Integer.valueOf(raTH[0])) ? 1 : 0;

			if (this.BIT_VECTOR_WIDTH > 1) {
				this.rain = (rainIndex >= Integer.valueOf(raTH[1])) ? 2
						: this.rain;
			}
		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;

			bitVector[0] = (rain > 0) ? 1.0 : bitVector[0];
			if (this.BIT_VECTOR_WIDTH > 1)
				bitVector[1] = (rain > 1) ? 1.0 : bitVector[1];
			return bitVector;

		}
	}

	private class Snow {

		private int BIT_VECTOR_WIDTH;

		// number of bits to set
		int snow;

		// Index legend for snow:
		// no_snow - 0
		// lt_snow - 1
		// mod_snow - 2
		// hvy_snow - 3
		// blowing_snow - 4
		int snowIndex;

		public Snow(boolean lt_snow, boolean mod_snow, boolean hvy_snow,
				boolean blowing_snow, String snowTH) {

			String[] snTH = snowTH.split(" ");
			BIT_VECTOR_WIDTH = snTH.length;

			this.snowIndex = (lt_snow) ? 1 : 0;
			this.snowIndex = (mod_snow) ? 2 : this.snowIndex;
			this.snowIndex = (hvy_snow) ? 3 : this.snowIndex;
			this.snowIndex = (blowing_snow) ? 4 : this.snowIndex;

			this.snow = (snowIndex >= Integer.valueOf(snTH[0])) ? 1 : 0;

			if (this.BIT_VECTOR_WIDTH > 1) {
				this.snow = (snowIndex >= Integer.valueOf(snTH[1])) ? 2
						: this.snow;
			}
		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;

			bitVector[0] = (snow > 0) ? 1.0 : bitVector[0];
			if (this.BIT_VECTOR_WIDTH > 1)
				bitVector[1] = (snow > 1) ? 1.0 : bitVector[1];
			return bitVector;

		}
	}

	private class Fog {

		private int BIT_VECTOR_WIDTH;

		// number of bits to set
		int fog;

		// Index fog legend:
		// no_fog - 0
		// fog - 1
		// ice_fog - 2
		// haze - 3
		int fogIndex;

		public Fog(boolean fog, boolean ice_fog, boolean haze, String fogTH) {

			String[] fTH = fogTH.split(" ");
			BIT_VECTOR_WIDTH = fTH.length;

			this.fogIndex = (fog) ? 1 : 0;
			this.fogIndex = (ice_fog) ? 2 : this.fogIndex;
			this.fogIndex = (haze) ? 3 : this.fogIndex;

			this.fog = (fogIndex >= Integer.valueOf(fTH[0])) ? 1 : 0;

			if (this.BIT_VECTOR_WIDTH > 1) {
				this.fog = (fogIndex >= Integer.valueOf(fTH[1])) ? 2 : this.fog;
			}
			if (fog || haze) {
				this.fog = 1;
			}

			if (ice_fog) {
				this.fog = 2;
			}

		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;
			bitVector[0] = (fog > 0) ? 1.0 : bitVector[0];
			if (this.BIT_VECTOR_WIDTH > 1)
				bitVector[1] = (fog > 1) ? 1.0 : bitVector[1];
			return bitVector;

		}
	}

	private class Wind {

		private int BIT_VECTOR_WIDTH;

		// number of bits to set
		int wind;
		// number of bits to set for wind direction
		int windDirection;

		public Wind(double wind, double windDirection, double windGust,
				String windTH) {

			String[] wiTH = windTH.split(" ");

			int length = wiTH.length;

			switch (wiTH.length) {
			case (1):
				this.BIT_VECTOR_WIDTH = 1;
				this.wind = (wind > Double.valueOf(wiTH[0])) ? 1 : 0;
				break;
			case (2):
				this.BIT_VECTOR_WIDTH = 2;
				this.wind = (wind > Double.valueOf(wiTH[0])) ? 1 : 0;
				this.wind = (wind > Double.valueOf(wiTH[1])) ? 2 : this.wind;
				break;
			case (7):
				this.BIT_VECTOR_WIDTH = 3;
				this.wind = (wind > Double.valueOf(wiTH[0])) ? 1 : 0;
				this.wind = (wind > Double.valueOf(wiTH[1])) ? 2 : this.wind;
				this.windDirection = ((windDirection >= Double.valueOf(wiTH[3])) && (windDirection <= Double
						.valueOf(wiTH[4]))) ? 1 : 0;

				if (Integer.valueOf(wiTH[2]) > 1) {
					this.windDirection = ((windDirection >= Double
							.valueOf(wiTH[5])) && (windDirection <= Double
							.valueOf(wiTH[6]))) ? 1 : this.windDirection;
				}
				break;
			default:
				LOGGER.fatal("illegal wind TH width");
				System.exit(1);
			}

		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;

			bitVector[0] = (this.wind > 0) ? 1.0 : bitVector[0];
			if (BIT_VECTOR_WIDTH > 1) {
				bitVector[1] = (this.wind > 1) ? 1.0 : bitVector[1];
			}
			// wind direction
			if (BIT_VECTOR_WIDTH == 3) {
				bitVector[2] = (this.windDirection == 1) ? 1.0 : 0.0;
			}

			return bitVector;

		}

	}

	private class WindExt {

		private int BIT_VECTOR_WIDTH = 8;

		// number of bit to set
		int wind;
		// number of bits to set for wind direction
		int windDirection;

		public WindExt(double wind, double windDirection, double windGust,
				String windExtTH) {

			// TODO: curentlly supporting only 8 directions
			int numOfDirections = Integer.valueOf(windExtTH);
		
			if (wind > 10.0 || windGust > 10.0){
				
				wind = (windDirection > 337.5 || windDirection < 22.5) ? 1 : this.wind;
				wind = (windDirection > 22.5 && windDirection < 67.5) ? 2 : this.wind;
				wind = (windDirection > 67.5 && windDirection < 112.5) ? 3 : this.wind;
				wind = (windDirection > 112.5 && windDirection < 157.5) ? 4 : this.wind;
				wind = (windDirection > 157.5 && windDirection < 202.5) ? 5 : this.wind;
				wind = (windDirection > 202.5 && windDirection < 247.5) ? 6 : this.wind;
				wind = (windDirection > 247.5 && windDirection < 292.5) ? 7 : this.wind;
				wind = (windDirection > 292.5 && windDirection < 337.5) ? 8 : this.wind;
			}
			
		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			if (this.wind > 0)
				bitVector[this.wind - 1] = 1.0;
			return bitVector;

		}

	}
	private class Hail {

		private int BIT_VECTOR_WIDTH;

		// number of bits to set
		int hail;

		// hail index legend:
		// 0 - no hail
		// 1 - lt hail
		// 2 - mod hail
		// 3 - hvy hail
		int hailIndex;

		public Hail(boolean lt_hail, boolean mod_hail, boolean hvy_hail,
				String hailTH) {

			String[] haTH = hailTH.split(" ");
			this.BIT_VECTOR_WIDTH = haTH.length;

			this.hailIndex = (lt_hail) ? 1 : 0;
			this.hailIndex = (mod_hail) ? 2 : this.hailIndex;
			this.hailIndex = (hvy_hail) ? 3 : this.hailIndex;

			this.hail = (hailIndex >= Integer.valueOf(haTH[0])) ? 1 : 0;
		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			bitVector[0] = this.hail;
			return bitVector;

		}
	}

}
