package com.dieselProjects.AccidentPredictionBrain.InputVector;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class APDateTime {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	private double negValue = cfg.inputNegativeValue; 

	private double[] dateTime;
	private DateTimeFormatter dtf_h = DateTimeFormatter
			.ofPattern("HH dd/MM/yyyy");

	public APDateTime(String[] line, APDateTimeConfiguration dateTimeConfiguration) {

		LocalDateTime dateTime = LocalDateTime.parse(line[0], dtf_h);
		
		// time of day:
		if (dateTimeConfiguration.getTODUse()){
			double[] timeOfDay = new TimeOfDay(dateTime.getHour()).get();
			this.dateTime = ArrayUtils.addAll(this.dateTime, timeOfDay);
		}
		// day of week:
		if (dateTimeConfiguration.getDOWUse()){
			double[] dayOfWeek = new DayOfWeek(
				dateTime.getDayOfWeek().getValue() - 1).get();
			this.dateTime = ArrayUtils.addAll(this.dateTime, dayOfWeek);
		}
		// month:
		if (dateTimeConfiguration.getMONUse()){
			double[] month = new Month(dateTime.getMonthValue() - 1).get();
			this.dateTime = ArrayUtils.addAll(this.dateTime, month);
		}
		// special days:
		if (dateTimeConfiguration.getSpecialDayUse()){
			double[] specialDay = new SpecialDay(line[2]).get();
			this.dateTime = ArrayUtils.addAll(this.dateTime, specialDay);
		}
			
	}

	public double[] get() {

		return this.dateTime;

	}

	private class TimeOfDay {

		/*
		 * [0] 0-6 [1] 7-10 morining rush [2] 11 - 15 [3] 16 - 20 evening rush
		 * [4] 21 - 23
		 */

		private int BIT_VECTOR_WIDTH = 5;

		private int timeOfDay;

		public TimeOfDay(int timeOfDay) {
			this.timeOfDay = timeOfDay;
		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;

			switch (timeOfDay) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				bitVector[0] = 1.0;
				break;
			case 7:
			case 8:
			case 9:
			case 10:
				bitVector[1] = 1.0;
				break;
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				bitVector[2] = 1.0;
				break;
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
				bitVector[3] = 1.0;
				break;
			case 21:
			case 22:
			case 23:
				bitVector[4] = 1.0;
			}
			return bitVector;

		}

	}

	private class DayOfWeek {

		/*
		 * [0] start week [1] end week [2] weekend
		 */
		private int BIT_VECTOR_WIDTH = 3;

		private int dayOfWeek;

		public DayOfWeek(int dayOfWeek) {
			this.dayOfWeek = dayOfWeek;
		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;
			
			switch (dayOfWeek) {
			case 0:
			case 1:
			case 2:
				bitVector[0] = 1.0;
				break;
			case 3:
			case 4:
				bitVector[1] = 1.0;
				break;
			case 5:
			case 6:
				bitVector[2] = 1.0;
				break;
			}
			return bitVector;

		}

	}

	private class Month {

		/*
		 * description: months are clustered into 4 groups: winter: Dec,Jan,Feb
		 * Spring: Mar,Apr,May,Jun Summer: Jul, Aug (mainly summer vacation)
		 * Fall: Sep,Oct,Nov
		 */

		private int BIT_VECTOR_WIDTH = 4;
		private int month;

		public Month(int month) {
			this.month = month;
		}

		public double[] get() {
			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			for (int i = 0; i < BIT_VECTOR_WIDTH; i++)
				bitVector[i] = negValue;

			switch (month) {
			case 11:
			case 0:
			case 1:
				bitVector[0] = 1.0;
				break;
			case 2:
			case 3:
			case 4:
			case 5:
				bitVector[1] = 1.0;
				break;
			case 6:
			case 7:
				bitVector[2] = 1.0;
				break;
			case 8:
			case 9:
			case 10:
				bitVector[3] = 1.0;
				break;
			}

			return bitVector;
		}
	}

	private class SpecialDay {

		private int BIT_VECTOR_WIDTH = 1;

		private int specialDay;

		public SpecialDay(String specialDay) {

			this.specialDay = Integer.valueOf(specialDay);

		}

		public double[] get() {

			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			bitVector[0] = (specialDay == 1) ? 1.0 : negValue;
			return bitVector;
		
		}
	}

}
