package com.dieselProjects.AccidentPredictionBrain.InputVector;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/*
 * Description:
 * 	load dataBase and key vector from input files:
 * 	currently - gsp_140_s.csv & gsp_140_S_KeyVector.csv, 
 * 
 * 	Receives numberOfTrainingTicks 
 * 	
 * 	Load gsp_140_s.csv and convert each line to MLDataPair
 * 
 * store 
 * 	MLDataSet trainingSet
 * 	MLDataSet testingSet
 * 	
 */
public class DataBase {

	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private MLDataSet trainingSet;
	private MLDataSet testingSet;
	private final APDateTimeConfiguration dateTimeCfg;
	private final APWeatherConditionConfiguration weatherConditionCfg;
	private final APLightConditionConfiguration lightConditionCfg;

	public DataBase(String dataBaseFile, int numberOfTrainingTicks) {

		this.trainingSet = new BasicMLDataSet();
		this.testingSet = new BasicMLDataSet();

		this.dateTimeCfg = new APDateTimeConfiguration();
		this.weatherConditionCfg = new APWeatherConditionConfiguration();
		this.lightConditionCfg = new APLightConditionConfiguration();

		init(dataBaseFile, numberOfTrainingTicks);

	}

	public DataBase(String dataBaseFile, int numberOfTrainingTicks,
			APDateTimeConfiguration dateTimeCfg,
			APWeatherConditionConfiguration weatherConditionCfg,
			APLightConditionConfiguration lightConditionCfg) {

		this.trainingSet = new BasicMLDataSet();
		this.testingSet = new BasicMLDataSet();

		this.dateTimeCfg = dateTimeCfg;
		this.weatherConditionCfg = weatherConditionCfg;
		this.lightConditionCfg = lightConditionCfg;

		init(dataBaseFile, numberOfTrainingTicks);

	}

	private void init(String dataBaseFile, int numberOfTrainingTicks) {

		int trainingSetCounter = 0;

		try {
			CSVReader reader = new CSVReader(new FileReader(dataBaseFile));
			//CSVWriter writer = new CSVWriter(new FileWriter("Res/inoutBit.csv"));

			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {

				MLData inputData = new BasicMLData(convertInput(nextLine));
				MLData idealData = new BasicMLData(convertIdeal(nextLine));

				double[] writeLine = ArrayUtils.addAll(convertInput(nextLine),
						convertIdeal(nextLine));
				String[] writeLineStr = new String[writeLine.length];
				for (int i = 0; i < writeLine.length; i++) {
					writeLineStr[i] = String.valueOf(writeLine[i]);
				}
				//writer.writeNext(writeLineStr);

				if (trainingSetCounter < numberOfTrainingTicks) {
					// store trainingSet:
					trainingSet.add(inputData, idealData);
				} else {
					// store testingSet:
					testingSet.add(inputData, idealData);
				}
				trainingSetCounter++;

			}
			reader.close();
			//writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	public MLDataSet getTrainingSet() {
		return trainingSet;
	}

	public MLDataSet getTestingSet() {
		return testingSet;
	}

	public int getInputWidth() {
		LOGGER.info("input width " + trainingSet.getInputSize());
		return trainingSet.getInputSize();
	}

	public int getIdealWidth() {
		return trainingSet.getIdealSize();
	}

	private double[] convertInput(String[] line) {

		double[] convertedInput = null;

		double[] dateTime = new APDateTime(line, dateTimeCfg).get();
		convertedInput = ArrayUtils.addAll(convertedInput, dateTime);

		double[] weatherCondition = new APWeatherCondition(line,
				weatherConditionCfg).get();
		convertedInput = ArrayUtils.addAll(convertedInput, weatherCondition);

		double[] lightCondition = new APLightCondition(line, lightConditionCfg)
				.get();
		convertedInput = ArrayUtils.addAll(convertedInput, lightCondition);

		return convertedInput;

	}

	private double[] convertIdeal(String[] line) {

		return new APIdeal(line, new APIdealConfiguration()).get();
	}

}
