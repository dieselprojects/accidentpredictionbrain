package com.dieselProjects.AccidentPredictionBrain.InputVector;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class APIdeal {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	
	
	private double negValue = cfg.inputNegativeValue; 
	
	private int BIT_VECTOR_WIDTH = 1;
	
	private int accidents;

	public APIdeal(String[] line, APIdealConfiguration idealConfiguration) {

		this.accidents = (Integer.valueOf(line[1]) >= idealConfiguration.getIdealTH()) ? 1 : 0;

	}

	public double[] get() {

		double[] bitVector = new double[BIT_VECTOR_WIDTH];
		bitVector[0] = (accidents == 1) ? 1.0 : negValue;
		return bitVector;
		
	}
}
