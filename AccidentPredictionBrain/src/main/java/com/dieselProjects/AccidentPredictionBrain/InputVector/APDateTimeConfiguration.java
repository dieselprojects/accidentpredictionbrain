package com.dieselProjects.AccidentPredictionBrain.InputVector;

public class APDateTimeConfiguration {

	private final static int DATE_TIME_USE_WIDTH = 4;
	
	 // dateTimeUse legend: 
	 // [0] time of day 
	 // [1] day of week 
	 // [2] month
	 // [3] special days
	private boolean[] dateTimeUse = new boolean[DATE_TIME_USE_WIDTH];

	// Clustering legend, 
	// TODO: think how to implements
	public String[] dateTimeClustering = new String[DATE_TIME_USE_WIDTH];

	public APDateTimeConfiguration(){
		// default configuration:
		this.dateTimeUse[0] = true;
		this.dateTimeUse[1] = true;
		this.dateTimeUse[2] = false;
		this.dateTimeUse[3] = false;
		
	}
	
	public void reset(){
		this.dateTimeUse[0] = true;
		this.dateTimeUse[1] = true;
		this.dateTimeUse[2] = false;
		this.dateTimeUse[3] = false;
	}
	
	// time of day
	public void setTODUse(boolean use){
		dateTimeUse[0] = use;
	}
	
	public boolean getTODUse(){
		return dateTimeUse[0];
	}
	
	// day of week
	public void setDOWUse(boolean use){
		dateTimeUse[1] = use;
	}
	
	public boolean getDOWUse(){
		return dateTimeUse[1];
	}
	
	// month:
	public void setMONUse(boolean use){
		dateTimeUse[2] = use;
	}
	
	public boolean getMONUse(){
		return dateTimeUse[2];
	}
	
	// specialDay
	public void setSpecialDayUse(boolean use){
		dateTimeUse[3] = use;
	}
	
	public boolean getSpecialDayUse(){
		return dateTimeUse[0];
	}
	
}
