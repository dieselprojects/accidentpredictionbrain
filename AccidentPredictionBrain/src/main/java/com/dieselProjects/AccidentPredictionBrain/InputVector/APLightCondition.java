package com.dieselProjects.AccidentPredictionBrain.InputVector;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class APLightCondition {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	private double negValue = cfg.inputNegativeValue; 

	double[] lightCondition;
	
	public APLightCondition(String[] line,
			APLightConditionConfiguration lightConditionConfiguration) {

		if (lightConditionConfiguration.getSunRiseUse()) {
			double[] sunRise = new SunRise(line[36].equals("1")).get();
			this.lightCondition = ArrayUtils.addAll(this.lightCondition,sunRise);
		}

		if (lightConditionConfiguration.getSunSetUse()) {
			double[] sunSet = new SunSet(line[37].equals("1")).get();
			this.lightCondition = ArrayUtils.addAll(this.lightCondition,sunSet);
		}
		
		if (lightConditionConfiguration.getLightUse()) {
			double[] light = new SunRise(line[38].equals("1")).get();
			this.lightCondition = ArrayUtils.addAll(this.lightCondition,light);
		}

		if (lightConditionConfiguration.getDarkUse()) {
			double[] dark = new SunSet(line[39].equals("1")).get();
			this.lightCondition = ArrayUtils.addAll(this.lightCondition,dark);
		}
	}
	
	public double[] get() {

		return this.lightCondition;

	}
	
	private class SunRise{
		
		private int BIT_VECTOR_WIDTH = 1;

		// number of bits to set
		boolean sunRise;

		public SunRise(boolean sunRise) {
			this.sunRise = sunRise;
		}
		
		public double[] get(){
			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			bitVector[0] = (this.sunRise) ? 1.0 : negValue;
			return bitVector;
		}
	}

	private class SunSet{
		
		private int BIT_VECTOR_WIDTH = 1;

		// number of bits to set
		boolean sunSet;

		public SunSet(boolean sunSet) {
			this.sunSet = sunSet;
		}
		
		public double[] get(){
			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			bitVector[0] = (this.sunSet) ? 1.0 : negValue;
			return bitVector;
		}
	}
	
	private class Light{
		
		private int BIT_VECTOR_WIDTH = 1;

		// number of bits to set
		boolean light;

		public Light(boolean light) {
			this.light = light;
		}
		
		public double[] get(){
			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			bitVector[0] = (this.light) ? 1.0 : negValue;
			return bitVector;
		}
	}
	
	private class Dark{
		
		private int BIT_VECTOR_WIDTH = 1;

		// number of bits to set
		boolean dark;

		public Dark(boolean dark) {
			this.dark = dark;
		}
		
		public double[] get(){
			double[] bitVector = new double[BIT_VECTOR_WIDTH];
			bitVector[0] = (this.dark) ? 1.0 : negValue;
			return bitVector;
		}
	}
}
