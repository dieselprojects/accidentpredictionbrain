package com.dieselProjects.AccidentPredictionBrain.InputVector;

public class APLightConditionConfiguration {

	private final static int LIGHT_USE_WIDTH = 4;
	
	 // lightUse legend: 
	 // [0] sun rise 
	 // [1] sun set 
	 // [2] light  
	 // [3] dark 
	private boolean[] lightUse = new boolean[LIGHT_USE_WIDTH];

	public APLightConditionConfiguration(){
	
		// default configuration
		// sun rise:
		lightUse[0] = false;
		// sun set:
		lightUse[1] = false;
		// light:
		lightUse[2] = false;
		// dark:
		lightUse[3] = false;
		
	}
	
	public void setAllToTrue(){
		
		// sun rise:
		lightUse[0] = true;
		// sun set:
		lightUse[1] = true;
		// light:
		lightUse[2] = true;
		// dark:
		lightUse[3] = true;
	}
	
	public void reset(){
		for (int i = 0; i < this.LIGHT_USE_WIDTH; i++)
			lightUse[i] = false;
	}
	
	
	public void setSunRiseUse(boolean use){
		lightUse[0] = use;
	}

	public boolean getSunRiseUse(){
		return lightUse[0];
	}
	
	public void setSunSetUse(boolean use){
		lightUse[1] = use;
	}

	public boolean getSunSetUse(){
		return lightUse[1];
	}
	
	public void setLightUse(boolean use){
		lightUse[2] = use;
	}

	public boolean getLightUse(){
		return lightUse[2];
	}
	
	public void setDarkUse(boolean use){
		lightUse[3] = use;
	}

	public boolean getDarkUse(){
		return lightUse[3];
	}

}
