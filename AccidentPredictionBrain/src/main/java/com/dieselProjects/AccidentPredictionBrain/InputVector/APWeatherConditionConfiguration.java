package com.dieselProjects.AccidentPredictionBrain.InputVector;

public class APWeatherConditionConfiguration {

	private final static int WEATHER_USE_WIDTH = 8;

	
	// weatherUse legend: 
	// [0] visibility 
	// [1] precipitation 
	// [2] rain 
	// [3] snow 
	// [4] fog
	// [5] wind
	// [6] wind ext' (8 wind directions)
	// [7] hail
	private boolean[] weatherUse = new boolean[WEATHER_USE_WIDTH];

	// Thresholds legend, space indicate two bit output, first value is
	// threshold for bit 0, second value indicate threshold for bit 1 
	// [0] visibility threshold 
	// [1] precipitation threshold
	// [2] rain
	// [3] snow
	// [4] fog
	// [5] wind
	// [6] wind ext
	// [7] hail
	private String[] weatherTH = new String[WEATHER_USE_WIDTH];

	public APWeatherConditionConfiguration(){
	
		// non ga implementation, default configuration
		// visibility:
		weatherUse[0] = false;
		// precipitation:
		weatherUse[1] = false;
		// rain:
		weatherUse[2] = false;
		// snow:
		weatherUse[3] = false;
		// fog:
		weatherUse[4] = false;
		// wind:
		weatherUse[5] = false;
		// wind ext:
		weatherUse[6] = false;
		// hail:
		weatherUse[7] = false;
	
		// visibility:
		weatherTH[0] = "3";
		// precipitation:
		weatherTH[1] = "0.05 0.25";
		
		// Legend for rain: // TODO: consider changing logic to support abs lt rain and others (don't consider frz)
		// no rain - 0
		// lt_rain - 1
		// mod_rain - 2
		// hvy_rain - 3
		// lt_frz_rain - 4
		// mod_frz_rain - 5
		// hvy_frz_rain - 6
		weatherTH[2] = "2 5";
		
		// Index legend for snow:
		// no_snow - 0
		// lt_snow - 1
		// mod_snow - 2
		// hvy_snow - 3
		// blowing_snow - 4
		weatherTH[3] = "2";
		
		// Index fog legend:
		// no_fog - 0
		// fog - 1
		// ice_fog - 2
		// haze - 3
		weatherTH[4] = "1";
		
		// TODO: only regular wind, add gust and direction
		weatherTH[5] = "10.0 15.0 2 110 180 270 340";
		
		// wind Ext, number of wind direction
		weatherTH[6] = "8";
		
		// hail
		// no hail - 0
		// lt hail - 1
		// mod hail - 2
		// hvy hail - 3
		weatherTH[7] = "1";
	}
	
	public void setAllToTrue(){
		// visibility:
		weatherUse[0] = true;
		// precipitation:
		weatherUse[1] = true;
		// rain:
		weatherUse[2] = true;
		// snow:
		weatherUse[3] = true;
		// fog:
		weatherUse[4] = true;
		// wind:
		weatherUse[5] = true;
		// wind ext:
		weatherUse[6] = true;
		// hail:
		weatherUse[7] = true;
	}
	
	public void resetUse(){
		for (int i = 0; i < this.WEATHER_USE_WIDTH; i++)
			weatherUse[i] = false;
	}
	
	// visibility:
	public void setVisibilityUse(boolean use){
		weatherUse[0] = use;
	}
	
	public void setVisibilityTH(String th){
		weatherTH[0] = th;
	}
	
	public boolean getVisibility(){
		return weatherUse[0];
	}

	public String getVisibilityTH(){
		return weatherTH[0];
	}

	// precipitation:
	public void setPrecipitationUse(boolean use){
		weatherUse[1] = use;
	}
	
	public void setPrecipitationTH(String th){
		weatherTH[1] = th;
	}
	
	public boolean getPrecipitation(){
		return weatherUse[1];
	}

	public String getPrecipitationTH(){
		return weatherTH[1];
	}

	// rain
	public void setRainUse(boolean use){
		weatherUse[2] = use;
	}
	
	public void setRainTH(String th){
		weatherTH[2] = th;
	}
	
	public boolean getRain(){
		return weatherUse[2];
	}

	public String getRainTH(){
		return weatherTH[2];
	}

	// snow
	public void setSnowUse(boolean use){
		weatherUse[3] = use;
	}
	
	public void setSnowTH(String th){
		weatherTH[3] = th;
	}
	
	public boolean getSnow(){
		return weatherUse[3];
	}

	public String getSnowTH(){
		return weatherTH[3];
	}

	// fog
	public void setFogUse(boolean use){
		weatherUse[4] = use;
	}
	
	public void setFogTH(String th){
		weatherTH[4] = th;
	}
	
	public boolean getFog(){
		return weatherUse[4];
	}

	public String getFogTH(){
		return weatherTH[4];
	}

	// wind
	public void setWindUse(boolean use){
		weatherUse[5] = use;
	}
	
	public void setWindTH(String th){
		weatherTH[5] = th;
	}
	
	public boolean getWind(){
		return weatherUse[5];
	}

	public String getWindTH(){
		return weatherTH[5];
	}
	
	// wind Ext
	public void setWindExtUse(boolean use){
		weatherUse[6] = use;
	}
	
	public void setWindExtTH(String th){
		weatherTH[6] = th;
	}
	
	public boolean getWindExt(){
		return weatherUse[6];
	}

	public String getWindExtTH(){
		return weatherTH[6];
	}
	
	// hail
	public void setHailUse(boolean use){
		weatherUse[7] = use;
	}
	
	public void setHailTH(String th){
		weatherTH[7] = th;
	}
	
	public boolean getHail(){
		return weatherUse[7];
	}

	public String getHailTH(){
		return weatherTH[7];
	}

	
}
