package com.dieselProjects.AccidentPredictionBrain;

public class APTypes {

	public enum APNetworkType {
		BASIC_FF(0), CUSTOM_FF_1(1), ELMAN(2), JORDAN(3);

		private final int mask;

		private APNetworkType(int mask) {
			this.mask = mask;
		}
		
		public static APNetworkType forInt(int id) {
	        for (APNetworkType  type : values()) {
	            if (type.mask == id) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Invalid Type id: " + id);
	    }
		public int getMask() {
			return mask;
		}

	}
	
	
	public enum APActivationType {
		BASIC_BP(0), RESILIENT(1);

		private final int mask;

		private APActivationType(int mask) {
			this.mask = mask;
		}
		
		public static APActivationType forInt(int id) {
	        for (APActivationType  type : values()) {
	            if (type.mask == id) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Invalid Type id: " + id);
	    }
		public int getMask() {
			return mask;
		}

	}
	
	public enum ROUTE{
		GSP,US206,US9,I80,US1,NJ17
	}
	

}
