package com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class GANetworkFactory extends AbstractCandidateFactory<NetworkParams> {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	
	public NetworkParams generateRandomCandidate(Random rng) {

		int[] nextCandidate = new int[cfg.numberOfParametersForGA];
		for (int i = 0; i < cfg.numberOfParametersForGA; i++) {

			int nextParam = 0;
			switch (i) {
			case 0:
				// networkType
				nextParam = rng.nextInt(3);
				break;
			case 1:
				// hidden layer width
				nextParam = rng.nextInt(10)+3;
				break;
			case 2:
				// activation function
				nextParam = rng.nextInt(2);
				break;
			default:
				break;
			}
			nextCandidate[i] = nextParam;

		}

		return new NetworkParams(nextCandidate);
	}

}
