package com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.ml.data.MLDataSet;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;
import com.dieselProjects.AccidentPredictionBrain.NeuralNetworks.APNNTrainer;
import com.dieselProjects.AccidentPredictionBrain.NeuralNetworks.APNNetworkResult;

public class GANetworkEvaluator implements FitnessEvaluator<NetworkParams> {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private Object lock1 = new Object();
	private final MLDataSet trainingSet;
	private final MLDataSet testingSet;

	public static double highestScore = 0.0;
	public static double trueRatio = 0.0;
	public static double falseRatio = 0.0;

	final int inputLayerWidth;
	final int outputLayerWidth;
	
	public GANetworkEvaluator(MLDataSet trainingSet, MLDataSet testingSet,int inputLayerWidth, int outputLayerWidth) {
		super();
		this.trainingSet = trainingSet;
		this.testingSet = testingSet;
		this.inputLayerWidth = inputLayerWidth;
		this.outputLayerWidth = outputLayerWidth;
		
	}

	public double getFitness(NetworkParams candidate,
			List<? extends NetworkParams> population) {

		double score = 0.0;

		// TODO: think if lock here is correct
		 synchronized(lock1){

		APNNTrainer networkEval = new APNNTrainer(candidate,inputLayerWidth,outputLayerWidth);
		networkEval.train(trainingSet);
		APNNetworkResult result = networkEval.test(testingSet);

		score = (result.getTrueHitPer() + result.getFalseHitPer()) / 2.0;

		if (score > highestScore) {
			highestScore = score;
			trueRatio = result.getTrueHitPer();
			falseRatio = result.getFalseHitPer();
		}

		print(candidate, result);
		}
		// return score
		return score;
	}

	public boolean isNatural() {
		// higher score counts
		return true;
	}

	private void print(NetworkParams params, APNNetworkResult result) {
		LOGGER.info("-------------------------------------------------");
		LOGGER.info("Results for GA iteration");
		params.print();
		LOGGER.info("Accidents hit ratio: " + result.getTrueHitPer());
		LOGGER.info("NON Accidents hit ratio: " + result.getFalseHitPer());
		LOGGER.info("Final Score: "
				+ (result.getTrueHitPer() + result.getFalseHitPer()) / 2.0);
		LOGGER.info("-------------------------------------------------");
	}

}
