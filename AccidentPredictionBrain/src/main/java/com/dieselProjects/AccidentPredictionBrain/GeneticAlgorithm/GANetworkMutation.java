package com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;

public class GANetworkMutation implements EvolutionaryOperator<NetworkParams>{
	
	private final GANetworkFactory networkFactory;
	
	private final Probability mutationProbability;
	
	public GANetworkMutation(GANetworkFactory networkFactory,
			Probability mutationProbability) {
		super();
		this.networkFactory = networkFactory;
		this.mutationProbability = mutationProbability;
	}


	public List<NetworkParams> apply(List<NetworkParams> selectedCandidates, Random rng) {
		
		List<NetworkParams> mutationPopulation = new ArrayList<NetworkParams>(selectedCandidates.size());
		for (NetworkParams nextCandidate : selectedCandidates){
			mutationPopulation.add(nextCandidate.mutate(rng, mutationProbability, networkFactory));
		}
		
		return mutationPopulation;
	}

}
