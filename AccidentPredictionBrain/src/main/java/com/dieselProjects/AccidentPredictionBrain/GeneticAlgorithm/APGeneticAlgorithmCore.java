package com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.ml.data.MLDataSet;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionEngine;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.GenerationCount;
import org.uncommons.watchmaker.framework.termination.TargetFitness;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.DataBase;

public class APGeneticAlgorithmCore {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private final GANetworkEvaluator networkEval;
	private final EvolutionEngine<NetworkParams> engine;

	public APGeneticAlgorithmCore(DataBase dataBase) {

		GANetworkFactory factory = new GANetworkFactory();

		List<EvolutionaryOperator<NetworkParams>> operators = new ArrayList<EvolutionaryOperator<NetworkParams>>(
				2);
		operators.add(new GANetworkMutation(factory, new Probability(cfg.mutationProbability)));
		operators.add(new GANetworkCrossover());

		networkEval = new GANetworkEvaluator(dataBase.getTrainingSet(), dataBase.getTestingSet(),dataBase.getInputWidth(),dataBase.getIdealWidth());
		engine = new GenerationalEvolutionEngine<NetworkParams>(factory,
				new EvolutionPipeline<NetworkParams>(operators), networkEval,
				new RouletteWheelSelection(), new MersenneTwisterRNG());

	}

	public NetworkParams evolve() {

		NetworkParams resultParams = engine.evolve(cfg.populationSize,
				cfg.elitism,
				new TargetFitness(cfg.fitnessTarget, networkEval.isNatural()),
				new GenerationCount(cfg.maxNumberOfGeneration));
		LOGGER.info("Finished GA execution, Result summary: ");
		resultParams.print();
		LOGGER.info("final score: " + networkEval.highestScore);
		LOGGER.info("Accidents hit ratio: " + networkEval.trueRatio);
		LOGGER.info("Non accidents hit ration: " + networkEval.falseRatio);

		return resultParams;

	}

}
