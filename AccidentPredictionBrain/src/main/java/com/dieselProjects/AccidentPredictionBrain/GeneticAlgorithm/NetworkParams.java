package com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.uncommons.maths.random.Probability;

import com.dieselProjects.AccidentPredictionBrain.APTypes.APActivationType;
import com.dieselProjects.AccidentPredictionBrain.APTypes.APNetworkType;
import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class NetworkParams {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	
	/*
	 * integer array to describe the network:
	 * index description:
	 * [0] - networkType 
	 * 		0: SimpleFF
	 * 		1: Elman
	 * 		2: Jorden
	 * [1] - number of neurons in hidden layer
	 * 		{3:12}
	 * [2] - Activation function
	 * 		0: Basic back propagation
	 * 		1: Resilient BP
	 */
	
	private int[] params;
	
	public NetworkParams(int[] params){
		this.params = params;
	}
	
	public NetworkParams mutate(Random rng, Probability mutationProbability, GANetworkFactory networkFactory){
		
		if (mutationProbability.nextEvent(rng)){
			return networkFactory.generateRandomCandidate(rng);
		} else {
			return this;
		}
		
	}
	
	public NetworkParams clone(){
		int[] clone = new int[params.length];
		
		for (int i = 0; i < params.length; i++)
			clone[i] = params[i];
		
		return new NetworkParams(clone);
		
	}
	
	public APNetworkType getNetworkType(){
		return APNetworkType.forInt(params[0]);
	}
	
	public int getHiddenLayerWidth(){
		return params[1];
	}
	
	public APActivationType getActivationType(){
		return APActivationType.forInt(params[2]);
	}
	
	public int[] getParams(){
		return params;
	}

	public void print(){
		LOGGER.info("Network Type: " + this.getNetworkType()); 
		LOGGER.info("Activation Type: " + this.getActivationType());
		LOGGER.info("Hidden layer width: " + this.getHiddenLayerWidth());
	}
	
}
