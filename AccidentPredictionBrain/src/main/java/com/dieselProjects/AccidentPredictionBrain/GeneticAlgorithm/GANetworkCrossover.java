package com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

public class GANetworkCrossover extends AbstractCrossover<NetworkParams>{

	protected GANetworkCrossover() {
		super(1);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected List<NetworkParams> mate(NetworkParams parent1, NetworkParams parent2,
			int numberOfCrossoverPoints, Random rng) {
	
		List<NetworkParams> offSpring = new ArrayList<NetworkParams>(2);
		NetworkParams offSpring1 = parent1.clone();
		NetworkParams offSpring2 = parent2.clone();

		for (int i = 0; i < numberOfCrossoverPoints; i++){
			int crossOverIndex = (rng.nextInt(parent1.getParams().length-1));
			NetworkParams temp = offSpring2.clone();
			
			for (int j = 0; j < crossOverIndex; j++){
			
					offSpring2.getParams()[j] = offSpring1.getParams()[j];
					offSpring1.getParams()[j] = temp.getParams()[j];

			}
			
		}
		
		offSpring.add(offSpring1);
		offSpring.add(offSpring2);
		return offSpring;

	}

}
