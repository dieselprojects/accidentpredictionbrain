package com.dieselProjects.AccidentPredictionBrain.NeuralNetworks;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.engine.network.activation.ActivationBiPolar;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationStep;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.mathutil.VectorAlgebra;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.neural.flat.FlatNetwork;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.neural.pattern.ElmanPattern;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.util.simple.EncogUtility;

import com.dieselProjects.AccidentPredictionBrain.APTypes.APActivationType;
import com.dieselProjects.AccidentPredictionBrain.APTypes.APNetworkType;
import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;
import com.dieselProjects.AccidentPredictionBrain.StorePE;
import com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm.NetworkParams;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APDateTimeConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APLightConditionConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APWeatherConditionConfiguration;

/*
 * Description:
 * Train and test Elman (recurrent) NeuralNetwork
 *  
 */
public class APNNTrainer {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	final BasicNetwork network;
	final APActivationType activationType;

	public APNNTrainer(int inputWidth, int idealWidth) {

		NNBuildFactory nnBuildFactory = new NNBuildFactory(cfg.networkType,
				inputWidth, idealWidth);
		network = nnBuildFactory.create();
		activationType = cfg.activationType;

	}

	public APNNTrainer(int inputWidth, int idealWidth, APNetworkType networkType) {

		NNBuildFactory nnBuildFactory = new NNBuildFactory(networkType,
				inputWidth, idealWidth);
		network = nnBuildFactory.create();
		activationType = cfg.activationType;

	}

	public APNNTrainer(NetworkParams networkParams, int inputWidth,
			int idealWidth) {

		NNBuildFactory nnBuildFactory = new NNBuildFactory(networkParams,
				inputWidth, idealWidth);
		network = nnBuildFactory.create();
		activationType = networkParams.getActivationType();

	}

	public void train(MLDataSet trainingSet) {

		MLTrain trainNetwork = null;

		// TODO: add different kinds of activation functions

		// for (int i = 0; i < 26; i++) {
		// for (int j = 0; j < 12; j++) {
		// network.enableConnection(0, i, j, false);
		// }
		// }
		switch (activationType) {
		case BASIC_BP:
			trainNetwork = new Backpropagation(network, trainingSet,
					cfg.learningRate, cfg.momentum);
			break;
		case RESILIENT:
			trainNetwork = new ResilientPropagation(network, trainingSet);
			break;
		default:
			break;
		}
		// EncogUtility.trainToError(trainNetwork, 0.0111);
		// LOGGER.info(new APNetworkStracture(network).showWeights());
		// VectorAlgebra va = new VectorAlgebra();
		// FlatNetwork flat = network.getFlat();
		int iteration = 0;
		do {
			trainNetwork.iteration();
			// //va.clampComponents(flat.getWeights(), 1);
			iteration++;
			LOGGER.trace("Training iteration [" + iteration
					+ "] Current ERROR " + trainNetwork.getError());
		} while (trainNetwork.isTrainingDone()
				|| iteration <= cfg.maxTrainingIterations);

		LOGGER.info("Done training, Training iteration [" + iteration
				+ "] ERROR " + trainNetwork.getError());
		trainNetwork.finishTraining();
		// save network:
		// EncogDirectoryPersistence.saveObject(new File("Res/encogNW.eg"),
		// network);
		// LOGGER.info("Finish training");
		// LOGGER.info(new APNetworkStracture(network).showWeights());
		// for (int i = 0; i < 21; i++) {
		// for (int j = 0; j < 12; j++) {
		// System.out.println(network.isConnected(0, i, j));
		// }
		// }
		// for (int i = 0; i < 26; i++){
		// for (int j = 0; j < 12; j++){
		// LOGGER.info(network.getWeight(0, i, j));
		// }
		// }
	}

	// TODO: currently first test only set the correct TH and the second test
	// actually test results, consider change
	public APNNetworkResult test(MLDataSet testSet) {

		APNNetworkResult results = new APNNetworkResult(cfg.outputTrueTH);

		for (MLDataPair inputSet : testSet)
			results.add(inputSet, network.compute(inputSet.getInput()));

		double outputTrueTH = results.getOptTrueTH();
		LOGGER.trace("Optimized TreshHold choosen : " + outputTrueTH);
		results = new APNNetworkResult(outputTrueTH);

		for (MLDataPair inputSet : testSet)
			results.add(inputSet, network.compute(inputSet.getInput()));

		results.print();
		return results;

	}

	public BasicNetwork getNetwork() {
		return this.network;
	}


}
