package com.dieselProjects.AccidentPredictionBrain.NeuralNetworks;

import java.util.Arrays;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class APNNetworkResult {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private int totalSamplesCnt;
	private int totalAccidentsCnt;

	private int matchTrueCnt;
	private int missTrueCnt;
	private double actualTrueAvg;

	private int matchFalseCnt;
	private int missFalseCnt;
	private double actualFalseAvg;

	private LinkedList<MLDataPair> missTrueInputList;
	private LinkedList<MLData> actOutputMissTrueList;

	private LinkedList<MLDataPair> missFalseInputList;
	private LinkedList<MLData> actOutputMissFalseList;

	private boolean setOptTrueTH;
	private double outputTrueTH;

	// Results:
	private double accidentHitRatio;
	private double nonAccidentHitRatio;
	private double totalScore;

	public APNNetworkResult(double outputTrueTH) {
		missTrueInputList = new LinkedList<MLDataPair>();
		actOutputMissTrueList = new LinkedList<MLData>();
		missFalseInputList = new LinkedList<MLDataPair>();
		actOutputMissFalseList = new LinkedList<MLData>();
		this.setOptTrueTH = false;
		this.outputTrueTH = outputTrueTH;
	}

	public void add(MLDataPair inputSet, MLData actOutput) {

		MLData inputVector = inputSet.getInput();
		MLData idealOutput = inputSet.getIdeal();

		totalSamplesCnt++;

		if (idealOutput.getData()[0] == 1.0) {
			totalAccidentsCnt++;
			if (actOutput.getData()[0] > outputTrueTH) {
				matchTrueCnt++;
			} else {
				missTrueCnt++;
				missTrueInputList.add(inputSet);
				actOutputMissTrueList.add(actOutput);
			}
			actualTrueAvg += actOutput.getData()[0];
		} else {

			if (actOutput.getData()[0] > outputTrueTH) {
				missFalseCnt++;
				missFalseInputList.add(inputSet);
				actOutputMissFalseList.add(actOutput);
			} else {
				matchFalseCnt++;
			}
			actualFalseAvg += actOutput.getData()[0];

		}
	}

	public void print() {
		LOGGER.info("Results Summery");

		LOGGER.trace("List of all missed Accidesnts(miss true) {[Input],[Ideal],[Actual]}:");
		for (int i = 0; i < missTrueInputList.size(); i++) {
			LOGGER.trace("{["
					+ Arrays.toString(missTrueInputList.get(i).getInputArray())
					+ "],["
					+ Arrays.toString(missTrueInputList.get(i).getIdealArray())
					+ "],["
					+ Arrays.toString(actOutputMissTrueList.get(i).getData())
					+ "]");
		}

		LOGGER.trace("List of all missed NON-Accidesnts(miss false) {[Input],[Ideal],[Actual]}:");
		for (int i = 0; i < missFalseInputList.size(); i++) {
			LOGGER.trace("{["
					+ Arrays.toString(missFalseInputList.get(i).getInputArray())
					+ "],["
					+ Arrays.toString(missFalseInputList.get(i).getIdealArray())
					+ "],["
					+ Arrays.toString(actOutputMissFalseList.get(i).getData())
					+ "]");
		}

		LOGGER.info("Total test input samples: "
				+ totalSamplesCnt
				+ ", Total accidents: "
				+ totalAccidentsCnt
				+ " ["
				+ String.format("%.2f", 100.0 * totalAccidentsCnt
						/ totalSamplesCnt) + "%]");

		this.accidentHitRatio = 100.0 * matchTrueCnt
				/ (matchTrueCnt + missTrueCnt);
		LOGGER.info("Accidents Hit results (accidents hit/total number of accidents): "
				+ matchTrueCnt
				+ "/"
				+ (matchTrueCnt + missTrueCnt)
				+ " ["
				+ String.format("%.2f", accidentHitRatio) + "%]");

		this.nonAccidentHitRatio = 100.0 * matchFalseCnt
				/ (matchFalseCnt + missFalseCnt);
		LOGGER.info("NON Accidents Hit results (accidents hit/total number of accidents): "
				+ matchFalseCnt
				+ "/"
				+ (matchFalseCnt + missFalseCnt)
				+ " ["
				+ String.format("%.2f", nonAccidentHitRatio) + "%]");

		LOGGER.info("Total result summery (total hits/total samples): "
				+ (matchTrueCnt + matchFalseCnt)
				+ "/"
				+ totalSamplesCnt
				+ "["
				+ String.format("%.2f", 100.0 * (matchTrueCnt + matchFalseCnt)
						/ (totalSamplesCnt)) + "%]");

		this.totalScore = (accidentHitRatio + nonAccidentHitRatio) / 2;
		LOGGER.info("Total Score ((Accidents Hit %) + (NonAccident Hit %))/2: ["
				+ String.format("%.2f", totalScore) + "%]");

	}

	public double getTrueHitPer() {
		return 100.0 * matchTrueCnt / (matchTrueCnt + missTrueCnt);
	}

	public double getFalseHitPer() {
		return 100.0 * matchFalseCnt / (matchFalseCnt + missFalseCnt);
	}

	public double getActualTrueAvg() {
		return actualTrueAvg;
	}

	public double getActualFalseAvg() {
		return actualFalseAvg;
	}

	public double getOptTrueTH() {

		if (setOptTrueTH == false) {
			actualTrueAvg = actualTrueAvg / totalAccidentsCnt;
			actualFalseAvg = actualFalseAvg
					/ (totalSamplesCnt - totalAccidentsCnt);

			LOGGER.info("Actual true average: " + actualTrueAvg);
			LOGGER.info("Actual false average: " + actualFalseAvg);

			// TODO: consider different approach to solve this issue:
			// each 'add' function call updates actualTrueAvg/False and need to
			// set actual avg at the last 'add' call
			setOptTrueTH = true;
		}
		return (this.getActualTrueAvg() + this.getActualFalseAvg())
				/ cfg.trueFalseFactor;
	}
	
	public double getScore(){
		return this.totalScore;
	}
	
	public double getAccidentsScore(){
		return this.accidentHitRatio;
	}
	
	public double getNonAccidentsScore(){
		return this.nonAccidentHitRatio;
	}

	public String[] getBenchMarkResultsCSV() {
		// results legend:
		// [0] Accident hit %
		// [1] Non accidents hit %
		// [2] Total score %
		return new String[] { String.format("%.2f", this.accidentHitRatio),
				String.format("%.2f",this.nonAccidentHitRatio),
				String.format("%.2f",this.totalScore) };
	}

}
