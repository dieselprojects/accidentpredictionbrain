package com.dieselProjects.AccidentPredictionBrain.NeuralNetworks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.engine.network.activation.ActivationBiPolar;
import org.encog.engine.network.activation.ActivationBipolarSteepenedSigmoid;
import org.encog.engine.network.activation.ActivationClippedLinear;
import org.encog.engine.network.activation.ActivationCompetitive;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationSteepenedSigmoid;
import org.encog.engine.network.activation.ActivationStep;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.pattern.ElmanPattern;
import org.encog.neural.pattern.HopfieldPattern;
import org.encog.neural.pattern.JordanPattern;
import org.encog.util.simple.EncogUtility;

import com.dieselProjects.AccidentPredictionBrain.APTypes.APNetworkType;
import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;
import com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm.NetworkParams;

public class NNBuildFactory {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	BasicNetwork network;
	final int inputLayerWidth;
	final int hiddenLayerWidth;
	final int outputLayerWidth;

	public NNBuildFactory(APNetworkType networkType, int inputLayerWidth,
			int outputLayerWidth) {

		this.inputLayerWidth = inputLayerWidth;
		this.hiddenLayerWidth = cfg.hiddenLayerWidth;
		this.outputLayerWidth = outputLayerWidth;

		switch (networkType) {
		case BASIC_FF:
			network = createBasicFFNetwork();
			break;
		case CUSTOM_FF_1:
			network = createCustom1FFNetwork(new APCustomFFNetworkConfiguration());
			break;
		case ELMAN:
			network = createElmanNetwork();
			network.reset();
			break;
		case JORDAN:
			network = createJordanNetwork();
			network.reset();
			break;
		default:
			LOGGER.fatal("ileagal network type " + networkType);
		}
	}

	public NNBuildFactory(NetworkParams networkParams, int inputLayerWidth,
			int outputLayerWidth) {

		this.inputLayerWidth = inputLayerWidth;
		this.hiddenLayerWidth = networkParams.getHiddenLayerWidth();
		this.outputLayerWidth = outputLayerWidth;

		switch (networkParams.getNetworkType()) {
		case BASIC_FF:
			network = createBasicFFNetwork();
			break;
		case CUSTOM_FF_1:
			network = createCustom1FFNetwork(new APCustomFFNetworkConfiguration());
			break;
		case ELMAN:
			network = createElmanNetwork();
			break;
		case JORDAN:
			network = createJordanNetwork();
			break;
		default:
			LOGGER.fatal("ileagal network type "
					+ networkParams.getNetworkType());
		}
		network.reset();

	}

	public BasicNetwork create() {
		return network;
	}

	private BasicNetwork createBasicFFNetwork() {

		BasicNetwork pattern = new BasicNetwork();

		pattern.addLayer(new BasicLayer(inputLayerWidth));
		pattern.addLayer(new BasicLayer(hiddenLayerWidth));
		pattern.addLayer(new BasicLayer(outputLayerWidth));
		pattern.getStructure().finalizeStructure();
		pattern.reset();

		return pattern;

	}

	private BasicNetwork createCustom1FFNetwork(
			APCustomFFNetworkConfiguration customFFNetworkConfiguration) {

		/*
		 * 2 hidden layers: first hidden layer create bottle necks with respect
		 * to timeDate, weather, light second hidden layer connects the inputs
		 */
		BasicNetwork pattern = new BasicNetwork();
		BasicLayer nextLayer = new BasicLayer(hiddenLayerWidth);
		nextLayer.setActivation(new ActivationSteepenedSigmoid());

		// input:
		pattern.addLayer(new BasicLayer(inputLayerWidth));
		
		// bottle neck:
		pattern.addLayer(new BasicLayer(customFFNetworkConfiguration
		.getCustom1LayerWidth()));
		
		pattern.addLayer(nextLayer);
		pattern.addLayer(new BasicLayer(customFFNetworkConfiguration
		.getCustom1LayerWidth()));
		
		pattern.addLayer(new BasicLayer(customFFNetworkConfiguration
		.getCustom1LayerWidth()));
		pattern.addLayer(new BasicLayer(hiddenLayerWidth));
		pattern.addLayer(new BasicLayer(hiddenLayerWidth/2));
		pattern.addLayer(new BasicLayer(hiddenLayerWidth/4));
		pattern.addLayer(new BasicLayer(hiddenLayerWidth/6));
		pattern.addLayer(new BasicLayer(outputLayerWidth));
		pattern.getStructure().finalizeStructure();
		pattern.reset();

		// disconnect all
		for (int i = 0; i < inputLayerWidth; i++) {
			for (int j = 0; j < customFFNetworkConfiguration
					.getCustom1LayerWidth(); j++) {
				pattern.enableConnection(0, i, j, false);
			}
		}

		// enable time/date:
		for (int i = 0; i < customFFNetworkConfiguration
				.getTimeDateInputWidth(); i++) {
			for (int j = 0; j < customFFNetworkConfiguration
					.getTimeDate1LayerWidth(); j++) {
				pattern.enableConnection(0, i, j, true);
			}
		}

		// weather
		for (int i = customFFNetworkConfiguration.getTimeDateInputWidth(); i < customFFNetworkConfiguration
				.getTimeDateInputWidth()
				+ customFFNetworkConfiguration.getWeatherInputWidth(); i++) {
			for (int j = customFFNetworkConfiguration.getTimeDate1LayerWidth(); j < customFFNetworkConfiguration
					.getTimeDate1LayerWidth()
					+ customFFNetworkConfiguration.getWeather1LayerWidth(); j++) {

				pattern.enableConnection(0, i, j, true);

			}
		}

		// light condition
		for (int i = customFFNetworkConfiguration.getTimeDateInputWidth()
				+ customFFNetworkConfiguration.getWeatherInputWidth(); i < customFFNetworkConfiguration
				.getTimeDateInputWidth()
				+ customFFNetworkConfiguration.getWeatherInputWidth()
				+ customFFNetworkConfiguration.getLightConditionInputWidth(); i++) {
			for (int j = customFFNetworkConfiguration.getTimeDate1LayerWidth()
					+ customFFNetworkConfiguration.getWeather1LayerWidth(); j < customFFNetworkConfiguration
					.getTimeDate1LayerWidth()
					+ customFFNetworkConfiguration
							.getLightCondition1LayerWidth()
					+ customFFNetworkConfiguration.getWeather1LayerWidth(); j++) {

				pattern.enableConnection(0, i, j, true);

			}
		}
		return pattern;

	}

	private BasicNetwork createElmanNetwork() {

		ElmanPattern pattern = new ElmanPattern();

		pattern.setActivationFunction(new ActivationTANH());
		pattern.setInputNeurons(inputLayerWidth);
		pattern.addHiddenLayer(hiddenLayerWidth);
		pattern.setOutputNeurons(outputLayerWidth);
		
		return (BasicNetwork) pattern.generate();

	}

	private BasicNetwork createJordanNetwork() {

		JordanPattern pattern = new JordanPattern();

		pattern.setActivationFunction(new ActivationSigmoid());
		pattern.setInputNeurons(inputLayerWidth);
		pattern.addHiddenLayer(hiddenLayerWidth);
		pattern.setOutputNeurons(outputLayerWidth);

		return (BasicNetwork) pattern.generate();

	}

	private BasicNetwork createJordanBiPolarNetwork() {

		JordanPattern pattern = new JordanPattern();

		pattern.setActivationFunction(new ActivationBiPolar());
		pattern.setInputNeurons(inputLayerWidth);
		pattern.addHiddenLayer(hiddenLayerWidth);
		pattern.setOutputNeurons(outputLayerWidth);

		return (BasicNetwork) pattern.generate();

	}

	private class APCustomFFNetworkConfiguration {

		int timeDateCount;
		int weatherCount;
		int lightConditionCount;

		int timeDate2Layer;
		int weather2Layer;
		int lightCondition2Layer;

		public APCustomFFNetworkConfiguration() {

			timeDateCount = 13;
			weatherCount = 9;
			lightConditionCount = 4;

			timeDate2Layer = 9;
			weather2Layer = 4;
			lightCondition2Layer = 2;

		}

		public int getCustomInputWidth() {
			return timeDateCount + weatherCount + lightConditionCount;
		}

		public int getTimeDateInputWidth() {
			return timeDateCount;
		}

		public int getWeatherInputWidth() {
			return weatherCount;
		}

		public int getLightConditionInputWidth() {
			return lightConditionCount;
		}

		public int getCustom1LayerWidth() {
			return timeDate2Layer + weather2Layer + lightCondition2Layer;
		}

		public int getTimeDate1LayerWidth() {
			return timeDate2Layer;
		}

		public int getWeather1LayerWidth() {
			return weather2Layer;
		}

		public int getLightCondition1LayerWidth() {
			return lightCondition2Layer;
		}

	}

}
