package com.dieselProjects.AccidentPredictionBrain.NeuralNetworks;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.neural.networks.BasicNetwork;

import au.com.bytecode.opencsv.CSVWriter;

import com.dieselProjects.AccidentPredictionBrain.APTypes.APNetworkType;
import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;
import com.dieselProjects.AccidentPredictionBrain.StorePE;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APDateTimeConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APLightConditionConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APWeatherConditionConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.DataBase;

/*
 * Description:
 * holds different scenarios for training and testing NN engine.
 * Scenario0: 
 * 	Use recurrent NN with one cluster
 */
public class TrainerScenarios {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	String dataBaseFile;

	public TrainerScenarios(String dataBaseFile) {
		this.dataBaseFile = dataBaseFile;
	}

	public void scenario0(DataBase dataBase) {

		APNNTrainer nNetworkTrainer = new APNNTrainer(dataBase.getInputWidth(),
				dataBase.getIdealWidth());

		nNetworkTrainer.train(dataBase.getTrainingSet());
		nNetworkTrainer.test(dataBase.getTestingSet());

	}

	public void scenario1() {
		/*
		 * description: check indicators effects over 3 different networks: 1.
		 * elman, 2. jordan, 3. customFF,
		 * 
		 * base indicators will be dateTime without additional month and special
		 * days. each indicator will train the network and test it, and will
		 * generate and store score according to equal accident/Non accident
		 * scoring system.
		 * 
		 * each iteration another config item is being added using
		 * configurations
		 */

		APDateTimeConfiguration dateTimeCfg = new APDateTimeConfiguration();
		APWeatherConditionConfiguration weatherConditionCfg = new APWeatherConditionConfiguration();
		APLightConditionConfiguration lightConditionCfg = new APLightConditionConfiguration();

		try {
			CSVWriter writer = new CSVWriter(new FileWriter(cfg.benchMarkFile));

			// ---------------------------------- //
			// - initial benchmark only TOD,DOW - //
			// ---------------------------------- //

			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "None", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "NONE", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "NONE", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// --------- //
			// - Month - //
			// --------- //

			dateTimeCfg.setMONUse(true);

			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Month", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Month", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Month", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			dateTimeCfg.reset();

			// ---------------- //
			// - Special Days - //
			// ---------------- //

			dateTimeCfg.setSpecialDayUse(true);

			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "SpecialDay",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "SpecialDay", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "SpecialDay", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			dateTimeCfg.reset();
			// -------------- //
			// - Visibility - //
			// -------------- //

			weatherConditionCfg.setVisibilityUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Visibility",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Visibility", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Visibility", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			weatherConditionCfg.resetUse();

			// ----------------- //
			// - Precipitation - //
			// ----------------- //

			weatherConditionCfg.setPrecipitationUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Precipitation",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Precipitation",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Precipitation",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);
			weatherConditionCfg.resetUse();

			// -------- //
			// - Rain - //
			// -------- //

			weatherConditionCfg.setRainUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Rain", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Rain", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Rain", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			weatherConditionCfg.resetUse();

			// -------- //
			// - Snow - //
			// -------- //

			weatherConditionCfg.setSnowUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Snow", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Snow", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Snow", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			weatherConditionCfg.resetUse();

			// ------- //
			// - Fog - //
			// ------- //

			weatherConditionCfg.setFogUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Fog", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Fog", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Fog", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			weatherConditionCfg.resetUse();

			// -------- //
			// - Wind - //
			// -------- //

			weatherConditionCfg.setWindUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Wind", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Wind", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Wind", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			weatherConditionCfg.resetUse();

			// ------------ //
			// - Sun Rise - //
			// ------------ //

			lightConditionCfg.setSunRiseUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "SunRise", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "SunRise", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "SunRise", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			lightConditionCfg.reset();

			// ----------- //
			// - Sun Set - //
			// ----------- //

			lightConditionCfg.setSunSetUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			lightConditionCfg.reset();

			// --------- //
			// - Light - //
			// --------- //

			lightConditionCfg.setLightUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Light", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Light", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Light", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			lightConditionCfg.reset();

			// -------- //
			// - Dark - //
			// -------- //

			lightConditionCfg.setDarkUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF, "Dark", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN, "Dark", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN, "Dark", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			lightConditionCfg.reset();

			LOGGER.info("Done scenario 1");
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	public void scenario2() {
		/*
		 * description: combining indicators:
		 */

		APDateTimeConfiguration dateTimeCfg = new APDateTimeConfiguration();
		APWeatherConditionConfiguration weatherConditionCfg = new APWeatherConditionConfiguration();
		APLightConditionConfiguration lightConditionCfg = new APLightConditionConfiguration();

		try {
			CSVWriter writer = new CSVWriter(new FileWriter(
					cfg.benchMarkCombineFile));

			// -------------------------------------------------- //
			// - SpecialDays, Visibility, Precipitation, SunSet - //
			// -------------------------------------------------- //

			dateTimeCfg.setSpecialDayUse(true);
			weatherConditionCfg.setVisibilityUse(true);
			weatherConditionCfg.setPrecipitationUse(true);
			lightConditionCfg.setSunSetUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF,
					"SpecialDays Visibility Precipitation SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN,
					"SpecialDays Visibility Precipitation SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN,
					"SpecialDays Visibility Precipitation SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);
			dateTimeCfg.reset();
			;
			weatherConditionCfg.resetUse();
			;
			lightConditionCfg.reset();
			;

			// ----------------------------------------------------------- //
			// - SpecialDays, Visibility, Precipitation, Rain, Snow, Fog - //
			// ----------------------------------------------------------- //

			dateTimeCfg.setSpecialDayUse(true);
			weatherConditionCfg.setVisibilityUse(true);
			weatherConditionCfg.setPrecipitationUse(true);
			weatherConditionCfg.setRainUse(true);
			weatherConditionCfg.setSnowUse(true);
			weatherConditionCfg.setFogUse(true);
			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF,
					"SpecialDays Visibility Precipitation Rain Snow Fog",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN,
					"SpecialDays Visibility Precipitation Rain Snow Fog",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN,
					"SpecialDays Visibility Precipitation Rain Snow Fog",
					dateTimeCfg, weatherConditionCfg, lightConditionCfg);
			dateTimeCfg.reset();
			;
			weatherConditionCfg.resetUse();
			;

			// ------------------------------------------------- //
			// - Visibility, Precipitation, SunSet, Rain, Wind - //
			// ------------------------------------------------- //

			weatherConditionCfg.setVisibilityUse(true);
			weatherConditionCfg.setPrecipitationUse(true);
			weatherConditionCfg.setRainUse(true);
			weatherConditionCfg.setWindUse(true);
			lightConditionCfg.setSunSetUse(true);

			// Basic FF:
			trainStep(writer, APNetworkType.BASIC_FF,
					"Visibility Precipitation Rain Wind SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Elman:
			trainStep(writer, APNetworkType.ELMAN,
					"Visibility Precipitation Rain Wind SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			// Jordan:
			trainStep(writer, APNetworkType.JORDAN,
					"Visibility Precipitation Rain Wind SunSet", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg);

			weatherConditionCfg.resetUse();
			;
			lightConditionCfg.reset();
			;

			LOGGER.info("Done scenario 2");
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	public void scenarion3(APNetworkType networkType, int iterationNumber) {
		/*
		 * Description: exec #PARAM number of iteration on each network and
		 * store Avg & Max result operate on networkType
		 */

		APDateTimeConfiguration dateTimeCfg = new APDateTimeConfiguration();
		APWeatherConditionConfiguration weatherConditionCfg = new APWeatherConditionConfiguration();
		APLightConditionConfiguration lightConditionCfg = new APLightConditionConfiguration();

		try {
			CSVWriter writer = new CSVWriter(new FileWriter(cfg.benchMarkFile));

			trainStepMaxAvg(writer, networkType, "None", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);

			// Visibility:
			weatherConditionCfg.setVisibilityUse(true);
			trainStepMaxAvg(writer, networkType, "Visibility", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			// Precipitation:
			weatherConditionCfg.setPrecipitationUse(true);
			trainStepMaxAvg(writer, networkType, "Precipitation", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			// Rain:
			weatherConditionCfg.setRainUse(true);
			trainStepMaxAvg(writer, networkType, "Rain", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			// Snow:
			weatherConditionCfg.setSnowUse(true);
			trainStepMaxAvg(writer, networkType, "Snow", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			// Fog:
			weatherConditionCfg.setFogUse(true);
			trainStepMaxAvg(writer, networkType, "Fog", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			// Wind:
			weatherConditionCfg.setWindUse(true);
			trainStepMaxAvg(writer, networkType, "Wind", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			// WindExt :
			weatherConditionCfg.setWindExtUse(true);
			trainStepMaxAvg(writer, networkType, "WindExt", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			// Hail:
			weatherConditionCfg.setHailUse(true);
			trainStepMaxAvg(writer, networkType, "Hail", dateTimeCfg,
					weatherConditionCfg, lightConditionCfg, iterationNumber);
			weatherConditionCfg.resetUse();

			LOGGER.info("Done scenario 3");
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	public void scenario4(APNetworkType networkType, int iterationNumber) {

		/*
		 * Description scenario4: train and store best network for predefined
		 * road sections, output should be compatible to APFarm
		 */

		// Configure

		// dateTime configuration:
		// TOD, DOW
		APDateTimeConfiguration dateTimeCfg = new APDateTimeConfiguration();

		// weather configuration:
		// Visibility - 2 bits, under 3 miles, under 1 mile
		// Rain - 2 bits, light and heavy rain
		// Snow - 1 bit, if exist
		// Fog - 1 bit, if exist
		// Wind - 1 bit, more than 10 miles/ph
		APWeatherConditionConfiguration weatherConditionCfg = new APWeatherConditionConfiguration();
		weatherConditionCfg.setVisibilityUse(true);
		weatherConditionCfg.setVisibilityTH("3 1");
		weatherConditionCfg.setRainUse(true);
		weatherConditionCfg.setRainTH("1 3");
		weatherConditionCfg.setSnowUse(true);
		weatherConditionCfg.setSnowTH("1");
		weatherConditionCfg.setFogUse(true);
		weatherConditionCfg.setFogTH("1");
		weatherConditionCfg.setWindUse(true);
		weatherConditionCfg.setWindTH("10.0");

		// don't use light condition
		APLightConditionConfiguration lightConditionCfg = new APLightConditionConfiguration();

		// Total input bit number (16)

		// Train, test and store networks, keep best network layouts
		trainBest(networkType, dateTimeCfg, weatherConditionCfg, lightConditionCfg, iterationNumber);
		

	}

	private void trainBest(APNetworkType networkType,
			APDateTimeConfiguration dateTimeCfg,
			APWeatherConditionConfiguration weatherConditionCfg,
			APLightConditionConfiguration lightConditionCfg, int iterationNumber) {

		BasicNetwork bestNetwork = null;
		APNNetworkResult bestResult = null;

		DataBase dataBase = new DataBase(dataBaseFile, cfg.numOfTrainingTick,
				dateTimeCfg, weatherConditionCfg, lightConditionCfg);

		for (int i = 0; i < iterationNumber; i++) {

			APNNTrainer nNetworkTrainer = new APNNTrainer(
					dataBase.getInputWidth(), dataBase.getIdealWidth(),
					networkType);

			nNetworkTrainer.train(dataBase.getTrainingSet());
			APNNetworkResult result = nNetworkTrainer.test(dataBase
					.getTestingSet());

			if ((bestResult == null)
					|| (bestResult.getScore() < result.getScore())) {
				bestNetwork = nNetworkTrainer.getNetwork();
				bestResult = result;
			}

		}
		// TODO: 
		// add multiple stores (array for different network types, locations, names)
		storeNetwork(bestNetwork, bestResult,cfg.location, cfg.networkName, cfg.networkHighLow);

	}

	private void trainStep(CSVWriter writer, APNetworkType networkType,
			String indicatorUsed, APDateTimeConfiguration dateTimeCfg,
			APWeatherConditionConfiguration weatherConditionCfg,
			APLightConditionConfiguration lightConditionCfg) {
		DataBase dataBase = null;
		// network description legend:
		// [0] network type
		// [1] added indicators (separate by space)
		String[] networkDescription = new String[2];

		networkDescription[0] = String.valueOf(networkType);
		networkDescription[1] = indicatorUsed;

		dataBase = new DataBase(dataBaseFile, cfg.numOfTrainingTick,
				dateTimeCfg, weatherConditionCfg, lightConditionCfg);

		APNNTrainer nNetworkTrainer = new APNNTrainer(dataBase.getInputWidth(),
				dataBase.getIdealWidth(), networkType);

		nNetworkTrainer.train(dataBase.getTrainingSet());
		APNNetworkResult result = nNetworkTrainer
				.test(dataBase.getTestingSet());

		storeBenchMark(writer, networkDescription,
				result.getBenchMarkResultsCSV());
	}

	private void trainStepMaxAvg(CSVWriter writer, APNetworkType networkType,
			String indicatorUsed, APDateTimeConfiguration dateTimeCfg,
			APWeatherConditionConfiguration weatherConditionCfg,
			APLightConditionConfiguration lightConditionCfg, int iterationNumber) {

		DataBase dataBase = null;

		double maxAccidents = 0.0;
		double maxNonAccidents = 0.0;
		double maxScore = 0.0;

		double avgAccidents = 0.0;
		double avgNonAccidents = 0.0;
		double avgScore = 0.0;

		// network description legend:
		// [0] network type
		// [1] added indicators (separate by space)
		String[] networkDescription = new String[2];

		networkDescription[0] = String.valueOf(networkType);
		networkDescription[1] = indicatorUsed;

		dataBase = new DataBase(dataBaseFile, cfg.numOfTrainingTick,
				dateTimeCfg, weatherConditionCfg, lightConditionCfg);

		for (int i = 0; i < iterationNumber; i++) {
			APNNTrainer nNetworkTrainer = new APNNTrainer(
					dataBase.getInputWidth(), dataBase.getIdealWidth(),
					networkType);
			Random randomGenerator = new Random();
			nNetworkTrainer.network.reset(randomGenerator.nextInt(100));

			nNetworkTrainer.train(dataBase.getTrainingSet());
			APNNetworkResult result = nNetworkTrainer.test(dataBase
					.getTestingSet());

			if (maxScore < result.getScore()) {
				maxAccidents = result.getAccidentsScore();
				maxNonAccidents = result.getNonAccidentsScore();
				maxScore = result.getScore();
			}
			avgAccidents += result.getAccidentsScore();
			avgNonAccidents += result.getNonAccidentsScore();
			avgScore += result.getScore();

		}

		avgAccidents /= iterationNumber;
		avgNonAccidents /= iterationNumber;
		avgScore /= iterationNumber;

		// create result string[] for csv
		String[] resultsForCsv = new String[] {
				String.format("%.2f", avgAccidents),
				String.format("%.2f", avgNonAccidents),
				String.format("%.2f", avgScore),
				String.format("%.2f", maxAccidents),
				String.format("%.2f", maxNonAccidents),
				String.format("%.2f", maxScore) };

		storeBenchMark(writer, networkDescription, resultsForCsv);
	}

	private void storeBenchMark(CSVWriter writer, String[] networkDescription,
			String[] results) {

		String[] line = (String[]) ArrayUtils.addAll(networkDescription,
				results);
		writer.writeNext(line);

	}

	private void storeNetwork(BasicNetwork networkToStore,
			APNNetworkResult resultToStore, String location,
			String networkName, String networkType) {

		StorePE storeNetwork = new StorePE(networkToStore, location,
				networkName, networkType, resultToStore);
		storeNetwork.store(cfg.farmDirLocation);
	}
}
