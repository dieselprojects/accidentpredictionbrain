package com.dieselProjects.AccidentPredictionBrain.NeuralNetworks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.neural.networks.BasicNetwork;

import com.dieselProjects.AccidentPredictionBrain.MainConfiguration;

public class APNetworkStracture {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private BasicNetwork network;
	
	public APNetworkStracture(BasicNetwork network){
		this.network = network;
	}
	
	public String showWeights(){
	    final StringBuilder result = new StringBuilder();

	    for (int layer = 0; layer < network.getLayerCount() - 1; layer++) {
	        int bias = 0;
	        if (network.isLayerBiased(layer)) {
	            bias = 1;
	        }

	        for (int fromIdx = 0; fromIdx < network.getLayerNeuronCount(layer)
	                + bias; fromIdx++) {
	            for (int toIdx = 0; toIdx < network.getLayerNeuronCount(layer + 1); toIdx++) {
	                String type1 = "", type2 = "";

	                if (layer == 0) {
	                    type1 = "I";
	                    type2 = "H" + (layer) + ",";
	                } else {
	                    type1 = "H" + (layer - 1) + ",";
	                    if (layer == (network.getLayerCount() - 2)) {
	                        type2 = "O";
	                    } else {
	                        type2 = "H" + (layer) + ",";
	                    }
	                }

	                if( bias ==1 && (fromIdx ==  network.getLayerNeuronCount(layer))) {
	                    type1 = "bias";
	                } else {
	                    type1 = type1 + fromIdx;
	                }

	                result.append(type1 + "-->" + type2 + toIdx
	                        + " : " + network.getWeight(layer, fromIdx, toIdx)
	                        + "\n");
	            }
	        }
	    }

	    return result.toString();
		
	}
}
