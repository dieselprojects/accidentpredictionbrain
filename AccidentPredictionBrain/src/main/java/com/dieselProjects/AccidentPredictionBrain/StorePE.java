package com.dieselProjects.AccidentPredictionBrain;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.encog.neural.networks.BasicNetwork;
import org.encog.persist.EncogDirectoryPersistence;

import com.dieselProjects.AccidentPredictionBrain.NeuralNetworks.APNNetworkResult;

public class StorePE {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private String networkName;
	private String networkType;
	private BasicNetwork network;
	private String location;
	APNNetworkResult result;
	

	public StorePE(BasicNetwork network, String location, String networkName, String networkType,
			APNNetworkResult result) {

		this.networkName = networkName;
		this.networkType = networkType;
		this.network = network;
		this.location = location;
		this.result = result;

	}

	public void store(String networkStoreDir) {

		// check if location exists, if not return
		APLocation locationBean = new APLocation(location);
		if (!locationBean.exists()) {
			LOGGER.warn("Location " + location
					+ " doesn't exists in APLocation, Store PE is canceled");
			return;
		}

		// Create directory:
		String newPEDirStr = networkStoreDir + "/" + location + "_" + networkType;
		File newPEDir = new File(newPEDirStr);
		if (newPEDir.exists()) {

			// remove directory in order to store new engine:
			String[] files = newPEDir.list();
			for (String file : files) {
				File nextFile = new File(newPEDir.getPath(), file);
				nextFile.delete();
			}
			newPEDir.delete();
			newPEDir.mkdir();
		} else {
			newPEDir.mkdir();
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Store location:
		try {
			PrintWriter locationOut = new PrintWriter(newPEDirStr
					+ "/location.csv");
			locationOut.println(locationBean.getLocationStr());
			locationOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}

		// Store network file:
		File encogNWFile = new File(newPEDirStr + "/encogNW.eq");
		EncogDirectoryPersistence.saveObject(encogNWFile, network);

		// Store time:
		LocalDateTime curTime = LocalDateTime.now();
		try (OutputStream createdTimeFile = new FileOutputStream(newPEDirStr
				+ "/created.ser");
				OutputStream buffer = new BufferedOutputStream(createdTimeFile);
				ObjectOutput output = new ObjectOutputStream(buffer);) {
			output.writeObject(curTime);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// Store benchMark results:
		APNetworkDescription networkDescriptionBean = new APNetworkDescription(
				networkName, networkType, result.getOptTrueTH(),
				result.getAccidentsScore(), result.getNonAccidentsScore(),
				result.getScore());
		
		try {
			PrintWriter descriptionOut = new PrintWriter(newPEDirStr
					+ "/description.csv");
			descriptionOut.println(networkDescriptionBean.getNetworkDescriptionStr());
			descriptionOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}

	}

	private class APNetworkDescription {

		private String name;
		private String type; // normal probability, high probability
		private String outputAccidentTH;
		private String benchMarkScore;
		private String benchMarkAccidentScore;
		private String benchMarkNonAccidentScore;

		public APNetworkDescription(String name, String type,
				double outputAccidentTH, double benchMarkScore,
				double benchMarkAccidentScore, double benchMarkNonAccidentScore) {
			this.name = name;
			this.type = type;
			this.outputAccidentTH = String.valueOf(outputAccidentTH);
			this.benchMarkScore = String.valueOf(benchMarkScore);
			this.benchMarkAccidentScore = String
					.valueOf(benchMarkAccidentScore);
			this.benchMarkNonAccidentScore = String
					.valueOf(benchMarkNonAccidentScore);
		}

		public String getNetworkDescriptionStr() {
			return this.name + "," + this.type + "," + this.outputAccidentTH
					+ "," + this.benchMarkScore + ","
					+ this.benchMarkAccidentScore + ","
					+ this.benchMarkNonAccidentScore;
		}

	}

	private class APLocation {
		/*
		 * Store the following: Lat/Lan, route, mile, // closest town
		 */

		private String startLoc;
		private String endLoc;
		private String route;
		private String mile;
		private String closestTown;
		
		private final LinkedList<String> routeDescription;
		
		private boolean exists = false;

		
		
		
		public APLocation(String location) {

			routeDescription = new LinkedList<String>();
			convert(location);

		}

		public String getLocationStr() {
			
			String routeDescriptionStr = "";
			for (String nextCor : this.routeDescription){
				routeDescriptionStr += "," + nextCor;
			}
			
			return this.startLoc + "," + endLoc + "," + this.route + "," + this.mile + ","
					+ this.closestTown + routeDescriptionStr;
		
		
		}

		public boolean exists() {
			return exists;
		}

		private void convert(String location) {

			// TODO: add routeDescription
			
			
			switch (location) {
			// Garden state parkway:
			case "gsp_140_mi":
				this.startLoc = "40.691174,-74.267393";
				this.endLoc = "40.759308,-74.208695";
				this.route = "gsp";
				this.mile = "140";
				this.closestTown = "union,nj";
				this.routeDescription.add("40.692738,-74.264358");
				this.routeDescription.add("40.694708,-74.261313");
				this.routeDescription.add("40.697699,-74.255638");
				this.routeDescription.add("40.700368,-74.250328");
				this.routeDescription.add("40.701185,-74.248841");
				this.routeDescription.add("40.702922,-74.246853");
				this.routeDescription.add("40.704417,-74.245852");
				this.routeDescription.add("40.708328,-74.245002");
				this.routeDescription.add("40.712387,-74.243667");
				this.routeDescription.add("40.717287,-74.239449");
				this.routeDescription.add("40.719713,-74.233516");
				this.routeDescription.add("40.723496,-74.229951");
				this.routeDescription.add("40.731361,-74.221894");
				this.routeDescription.add("40.738892,-74.216765");
				this.routeDescription.add("40.744686,-74.213549");
				this.routeDescription.add("40.752146,-74.211349");
				this.routeDescription.add("40.754481,-74.210364");
				this.exists = true;
				break;
			case "gsp_145_mi":
				this.startLoc = "40.759308,-74.208695";
				this.endLoc = "40.807686,-74.182305";
				this.route = "gsp";
				this.mile = "145";
				this.closestTown = "ampere,nj";
				this.routeDescription.add("40.764991,-74.204930");
				this.routeDescription.add("40.767680,-74.204232");
				this.routeDescription.add("40.772138,-74.204597");
				this.routeDescription.add("40.773839,-74.204172");
				this.routeDescription.add("40.775264,-74.202897");
				this.routeDescription.add("40.777171,-74.201289");
				this.routeDescription.add("40.779791,-74.200894");
				this.routeDescription.add("40.780112,-74.200955");
				this.routeDescription.add("40.782571,-74.201198");
				this.routeDescription.add("40.783697,-74.200985");
				this.routeDescription.add("40.787051,-74.198831");
				this.routeDescription.add("40.788775,-74.196585");
				this.routeDescription.add("40.791440,-74.192853");
				this.routeDescription.add("40.795460,-74.191609");
				this.routeDescription.add("40.797597,-74.190698");
				this.routeDescription.add("40.800514,-74.187482");
				this.routeDescription.add("40.804488,-74.184295");
				this.exists = true;
				break;
			case "i80_55_mi":
				this.startLoc = "40.90548,-74.18892";
				this.endLoc = "40.904,-74.11585";
				this.route = "i80";
				this.mile = "55";
				this.closestTown = "paterson,nj";
				// route description:
				this.routeDescription.add("40.908409,-74.183167");
				this.routeDescription.add("40.908278,-74.173989");
				this.routeDescription.add("40.905867,-74.161460");
				this.routeDescription.add("40.903051,-74.157709");
				this.routeDescription.add("40.901335,-74.152388");
				this.routeDescription.add("40.900409,-74.145460");
				this.routeDescription.add("40.901248,-74.139604");
				this.routeDescription.add("40.901933,-74.134819");
				this.routeDescription.add("40.901181,-74.128746");
				this.routeDescription.add("40.901065,-74.126118");
				this.routeDescription.add("40.903331,-74.119266");
				this.routeDescription.add("40.903861,-74.116778");
				this.exists = true;
				break;
				
			default:
				System.err.println("Error storing, no location is configure for " + location);
				System.exit(1);
				break;

			}

		}

	}

}
