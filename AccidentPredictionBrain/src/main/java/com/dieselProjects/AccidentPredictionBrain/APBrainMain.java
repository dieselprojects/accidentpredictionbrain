package com.dieselProjects.AccidentPredictionBrain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm.APGeneticAlgorithmCore;
import com.dieselProjects.AccidentPredictionBrain.GeneticAlgorithm.NetworkParams;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APDateTimeConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APLightConditionConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.APWeatherConditionConfiguration;
import com.dieselProjects.AccidentPredictionBrain.InputVector.DataBase;
import com.dieselProjects.AccidentPredictionBrain.NeuralNetworks.TrainerScenarios;

public class APBrainMain {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	public static void main(String[] args) {

		String startProgramString = "\n******************************************\n"
				+ "** Start Accident prediction NN trainer **\n"
				+ "******************************************\n";
		LOGGER.info(startProgramString);
		LOGGER.info(cfg.print());

		String dataBaseFile = null;
		switch (cfg.routeForUse){
		case GSP:
			dataBaseFile = cfg.routeFileLocation[0];
			break;
		case US206:
			dataBaseFile = cfg.routeFileLocation[1];
			break;
		case US9:
			dataBaseFile = cfg.routeFileLocation[2];
			break;
		case I80:
			dataBaseFile = cfg.routeFileLocation[3];
			break;
		case US1:
			dataBaseFile = cfg.routeFileLocation[4];
			break;
		case NJ17:
			dataBaseFile = cfg.routeFileLocation[5];
			break;
		}
		
		
		// load DataBase

		if (cfg.useGA) {

			DataBase dataBase = new DataBase(dataBaseFile,
					cfg.numOfTrainingTick);
			APGeneticAlgorithmCore gaAlgo = new APGeneticAlgorithmCore(dataBase);
			NetworkParams result = gaAlgo.evolve();

		} else {

			TrainerScenarios scenarios = new TrainerScenarios(dataBaseFile);

			APDateTimeConfiguration dateTimeCfg = new APDateTimeConfiguration();
			APWeatherConditionConfiguration weatherConditionCfg = new APWeatherConditionConfiguration();
			APLightConditionConfiguration lightConditionCfg = new APLightConditionConfiguration();
			DataBase dataBase = null;
			
			switch (cfg.scenario) {
			case (0):
				dateTimeCfg.setMONUse(true);
				dateTimeCfg.setSpecialDayUse(true);;
				weatherConditionCfg.setAllToTrue();
				lightConditionCfg.setAllToTrue();

				dataBase = new DataBase(dataBaseFile,
						cfg.numOfTrainingTick, dateTimeCfg,
						weatherConditionCfg, lightConditionCfg);
				scenarios.scenario0(dataBase);
				break;
			case (1):
				scenarios.scenario1();
				break;
			case (2):
				scenarios.scenario2();
				break;
			case (3):
				scenarios.scenarion3(cfg.networkType, cfg.iterationForScenario);
				break;
			case (4):
				scenarios.scenario4(cfg.networkType, cfg.iterationForScenario);
				break;
			case (5):
				/*
				 * special case of scenario 1, customize indication configuration here:
				 */
				dateTimeCfg.reset();;
				weatherConditionCfg.setAllToTrue();
				weatherConditionCfg.setWindUse(false);
				weatherConditionCfg.setHailUse(false);
				lightConditionCfg.setSunSetUse(true);;

				dataBase = new DataBase(dataBaseFile,
						cfg.numOfTrainingTick, dateTimeCfg,
						weatherConditionCfg, lightConditionCfg);
				scenarios.scenario0(dataBase);

				break;
			default:
				LOGGER.error("illegal scenario " + cfg.scenario);
				break;
			}

		}

		System.exit(0);
	}
}
